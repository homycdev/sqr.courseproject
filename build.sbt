name := "petmeeter"

version := "0.1"

scalaVersion := "2.13.3"

val slickVer = "3.3.2"
val jdbcVer = "42.2.18.jre7"
val circeVersion = "0.13.0"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,

  "com.typesafe.akka" %% "akka-actor" % "2.6.10",
  "com.typesafe.akka" %% "akka-stream" % "2.6.10",
  "com.typesafe.akka" %% "akka-http" % "10.2.1",
  "de.heikoseeberger" %% "akka-http-circe" % "1.35.2",

  "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.10" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.1" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test,

  "com.softwaremill.akka-http-session" %% "core" % "0.5.11",

  "com.typesafe.slick" %% "slick-codegen" % slickVer,
  "com.typesafe.slick" %% "slick" % slickVer,
  "org.postgresql" % "postgresql" % jdbcVer,
  "com.h2database" % "h2" % "1.4.200",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0",
  "org.flywaydb" % "flyway-core" % "7.3.1" % Test,
  "org.liquibase" % "liquibase-core" % "4.2.0",
  "ch.megard" %% "akka-http-cors" % "1.1.1"
)

enablePlugins(
  JavaAppPackaging,
  AssemblyPlugin
)

assemblyJarName in assembly := "app.jar"
mainClass in assembly := Some("petmeeter.petmeeterHttp")
test in assembly := {}
