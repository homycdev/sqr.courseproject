[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=code_smells)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=ncloc)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=security_rating)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=sqale_index)](https://sonarcloud.io/dashboard?id=sqr.courseproject) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sqr.courseproject&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=sqr.courseproject)


# SQR project
Pet Meeter is simple app to make meetings with other pet owners and walk with their pets. 

Link to front-end:  https://gitlab.com/homycdev/sqr.courseprojectfrontend

##Project stack:

- akka http: Backend server
- circe: JSON Realisation
- slick: JDBC layer
- scala-mock: Testing
- scala-test: Testing
- scala-bcrypt: For encryption
- akka-http-cors: For CORS


Project is deployed at http://sqr-stage-833259366.eu-central-1.elb.amazonaws.com

#### Backend API
| Desc      | Verb | Route | PAYLOAD |
| ----------- | ----------- | ----------- | ----------- |
| sign up a new user | POST | `/register` | **requested body** -> {"email": string, "password": string }; **response body** -> {} |
| sign in | POST | `/login` | **requested body** -> {"email": string, "password": string } |
| get group by id | GET | `/groups/{ID}` | **requested parameters** -> "ID": Long; **response body(code 200)** -> {"id": Long, "name": string, "description": string, "founderId": Long, "topics": [ string ] }; **response body(code 404)** ->{"message": "No group exists matching provided data!"} |
| get topics of the group by id | GET | `/groups/{ID}/topics` | **requested parameters** -> "ID": Long; **response body(code 200)** -> [ {"id": Long, "name": string} ]; **response body(code 404)** ->{"message": "No group exists matching provided data!"} |
| get comments of the group by id | GET | `/groups/{ID}/comments` | **requested parameters** -> "ID": Long; **response body(code 200)** -> [ {"id": Long, "groupid": Long, "userid": Long, "text": string, "timeat": ""/timestamp} ]; **response body(code 404)** ->{"message": "No group exists matching provided data!"} |
| add new comment to a group by id | POST | `/groups/{ID}/comments/add` | **requested parameter** -> "ID": Long, **requested body** "-> {"text": string}; **response (code 200)** -> True; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response body(code 404)** ->{"message": "No group exists matching provided data!"} |
| add new meeting to a group by id | POST | `/groups/{ID}/meetings` | **requested parameter** -> "ID": Long; **requested body** -> [ string ]];  **response (code 200)** -> True; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response body(code 404)** ->{"message": "No group exists matching provided data!"} |
| get all the groups | GET | `/groups/all` | **response body(code 200)** -> [ {"id": Long, "name": string, "description": string, "founderId": Long, "topics": [ string ] } ] |
| get groups by name | GET | `/groups?name={NAME}` | **requested parameter** -> "NAME": string; **response body** -> [ {"id": Long, "name": string, "description": string, "founderId": Long, "topics": [ string ] } ] |
| create new group | POST | `/groups/create` | **requested body** -> {"id": ""/Long, "name": string, "description": string, "founderId": Long, "topics": [ string ] }; **response (code 200)** -> {"ID": Long}; **response (code 403)** -> The supplied authentication is not authorized to access this resource; |
| get groups by topics | GET | `/groups/discoverByTopics?topic={TOPIC}` | **requested parameter** -> "TOPIC": string; **response body(code 200)** -> [ {"id": Long, "name": string, "description": string, "founderId": Long, "topics": [ string ] } ]  |
| get all users | GET | `/users/all` | **response body(code 200)** -> [ {"id": ""/Long, "email": ""/string, "firstname": ""/string, "lastname": ""/string, "topics": ""/[ "id": Long, "name": string ], "groups": [ "id": Long, "groupname": string, "groupdesc": string, "founderid": Long ] } ] |
| get user by id | GET | `/users/{ID}` | **requested parameter** -> "ID": Long; **response body(code 200)** -> {"id": Long, "email": string, "firstname": ""/string, "lastname": ""/string, "topics": ""/[ {"id": Long, "name": string} ], "groups": ""/[ {"id": Long, "groupname": string, "groupdesc": string, "fouiderid": Long} ]}; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| get topics of the user by id | GET | `/users/{ID}/topics` | **requested parameter** -> "TOPIC": string; **requested body(code 200)** -> [ {"id": string, "name": string} ]; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| get groups of the user by id | GET | `/users/{ID}/groups` | **requested parameter** -> "TOPIC": string; **requested body(code200)** -> [ {"id": string, "groupname": string, "groupdesc": string, "founderid": Long} ]; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| get messages of the users by id | GET | `/users/{ID}/messages` | **requested parameter** -> "TOPIC": string; **response body(code 200** -> [ {"id": Long, "senderid": Long, "receiverid": Long, "text": string, "timeat": ""/timestamp} ]; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| get notifications of the user by id | GET | `/users/{ID}/notifications` | **requested parameter** -> "TOPIC": string; **response body(code 200)** ->[ {"id": Long, "groupid": Long, "userid": Long, "text": string, "timeat: ""/timestamp} ]; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| update user by id | POST | `/users/{ID}/update` | **requested parameter** -> "TOPIC": string; **requested body** -> {"id": ""/Long, "email": ""/string, "firstname": ""/string, "lastname": ""/string, "topics": ""/[ string ], "groups": ""/[ string ]}; **response body(code 200)** -> {"id": Long, "email": string, "firstname": ""/string, "lastname": ""/string, "topics": ""/[ string ], "groups": ""/[ string ]}; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| update topics of the user by id | POST | `/users/{ID}/updateTopics` | **requested parameter** -> "TOPIC": string; **requested body** -> [ string ];  **response (code 200)** -> [ string ]  **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| user with ID joins group with GROUPID | POST | `/users/{ID}/joinGroup?groupId={GROUPID}` | **requested parameter** -> "TOPIC": string; **response(code 200)** -> True; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
| user with ID leave group with GROUPID | POST | `/users/{ID}/leaveGroup?groupId={GROUPID}` | **requested parameter** -> "TOPIC": string; **response(code 200)** -> True; **response (code 403)** -> The supplied authentication is not authorized to access this resource; **response (code 404)** -> {"message": "No user exists matching provided data!"} |
