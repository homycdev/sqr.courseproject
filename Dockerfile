FROM openjdk:8-jre-alpine

COPY target/scala-2.13/app.jar /usr/src/myapp/app.jar
WORKDIR /usr/src/myapp
EXPOSE 80

CMD java -jar app.jar