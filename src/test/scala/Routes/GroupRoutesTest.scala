package Routes

import akka.http.javadsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import petmeeter.data.Tables.{Comment, Topic}
import petmeeter.routes.{AuthRoutes, ExceptionResponse, GroupRoutes, PetMeeter}
import petmeeter.services.auth.AuthServiceImpl
import petmeeter.services.groups.{GroupServiceImpl, GroupsRepr}
import petmeeter._

import scala.concurrent.Future

class GroupRoutesTest
    extends SampleRepo
    with AsyncMockFactory
    with ScalatestRouteTest
    with Matchers {
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  behavior of "http://localhost:8080/groups/all"

  it should "successfully get all groups" in {
    (mockGroupService.getAllGroups _)
      .expects()
      .returns(
        Future.successful(Seq(groupRepr, groupRepr.copy(name = "anotherGroup")))
      )

    Get("/groups/all") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[GroupsRepr]] shouldBe Seq(
        groupRepr,
        groupRepr.copy(name = "anotherGroup")
      )
    }

  }

  behavior of "http://localhost:8080/groups/{groupId}"

  it should "successfully get group by its id" in {
    (mockGroupService.getGroupById _)
      .expects(1)
      .returns(Future.successful(groupRepr))

    Get("/groups/1") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[GroupsRepr] shouldBe groupRepr
    }
  }

  it should "fail if no group to be found" in {
    (mockGroupService.getGroupById _)
      .expects(10)
      .returns(Future.failed(GroupNotFoundException()))

    Get("/groups/10") ~> route ~> check {
      status shouldBe StatusCodes.NOT_FOUND
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No group exists matching provided data!"
      )
    }
  }

  behavior of "http://localhost:8080/groups/{groupId}/topics"

  it should "successfully return group's topics" in {

    (mockGroupService.getGroupTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))
    Get("/groups/1/topics") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Topic]] shouldBe SampleTopics.take(2)

    }

  }

  it should "successfully return an empty sequence if group has no topics" in {

    (mockGroupService.getGroupTopicsById _)
      .expects(1)
      .returns(Future.successful(Seq()))
    Get("/groups/1/topics") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Topic]] shouldBe Seq()

    }

  }

  it should "fail if no group to be found" in {

    (mockGroupService.getGroupTopicsById _)
      .expects(10)
      .returns(Future.failed(GroupNotFoundException()))
    Get("/groups/10/topics") ~> route ~> check {
      status shouldBe StatusCodes.NOT_FOUND
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No group exists matching provided data!"
      )

    }

  }

  behavior of "http://localhost:8080/groups/{groupId}/comments"

  it should "successfully return group's topics" in {

    (mockGroupService.getAllComments _)
      .expects(1)
      .returns(Future.successful(SampleComments.take(2)))
    Get("/groups/1/comments") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Comment]] shouldBe SampleComments.take(2)

    }

  }

  it should "successfully return an empty sequence if group has no comments" in {

    (mockGroupService.getAllComments _)
      .expects(1)
      .returns(Future.successful(Seq()))
    Get("/groups/1/comments") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Comment]] shouldBe Seq()

    }

  }

  it should "fail if no group to be found" in {

    (mockGroupService.getAllComments _)
      .expects(10)
      .returns(Future.failed(GroupNotFoundException()))
    Get("/groups/10/comments") ~> route ~> check {
      status shouldBe StatusCodes.NOT_FOUND
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No group exists matching provided data!"
      )

    }

  }

  behavior of "http://localhost:8080/groups?name={query}"

  it should "successfully find a group matching provided query" in {

    (mockGroupService.findGroupsByName _)
      .expects("%groupname%")
      .returns(Future.successful(Seq(groupRepr)))

    Get("/groups?name=%25groupname%25") ~> route ~> check {

      status shouldBe StatusCodes.OK

      responseAs[Seq[GroupsRepr]] shouldBe Seq(groupRepr)
    }

  }

  it should "return an empty list if not group matchers" in {

    (mockGroupService.findGroupsByName _)
      .expects("%randomname%")
      .returns(Future.successful(Seq()))

    Get("/groups?name=%25randomname%25") ~> route ~> check {

      status shouldBe StatusCodes.OK

      responseAs[Seq[GroupsRepr]] shouldBe Seq()
    }

  }

  behavior of "http://localhost:8080/groups/{groupId}/discoverByTopics?topic={topic}"

  it should "successfully find a group matching provided topics" in {

    (mockGroupService.findGroupsByTopics _)
      .expects(Seq("games", "sport"))
      .returns(Future.successful(Seq(groupRepr)))
    Get("/groups/discoverByTopics?topic=sport&topic=games") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[GroupsRepr]] shouldBe Seq(groupRepr)
    }

  }

  it should "return an empty list if not group matchers" in {

    (mockGroupService.findGroupsByTopics _)
      .expects(Seq("C++"))
      .returns(Future.successful(Seq()))

    Get("/groups/discoverByTopics?topic=C%2B%2B") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[GroupsRepr]] shouldBe Seq()
    }

  }

  behavior of "http://localhost:8080/groups/{groupId}/comments/add"

  it should "successfully add a comment" in {
    (mockGroupService.addComment _)
      .expects("This is a new comment", 1, 1)
      .returns(Future.successful(true))

    Post("/groups/1/comments/add", "This is a new comment").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.OK
    }
  }

  it should "fail if no authHeader provided" in {
    Post("/groups/1/comments/add", "This is a new comment") ~> route ~> check {
      status shouldBe StatusCodes.FORBIDDEN
    }
  }

  it should "fail if user doesn't exist" in {
    (mockGroupService.addComment _)
      .expects("This is a new comment", 1, 10)
      .returns(Future.failed(UserNotFoundException()))

    Post("/groups/1/comments/add", "This is a new comment").withHeaders(
      Seq(authHeader(10))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NOT_FOUND
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }
  }

  it should "fail if group doesn't exist" in {
    (mockGroupService.addComment _)
      .expects("This is a new comment", 10, 1)
      .returns(Future.failed(GroupNotFoundException()))

    Post("/groups/10/comments/add", "This is a new comment").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NOT_FOUND
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No group exists matching provided data!"
      )
    }
  }

  behavior of "http://localhost:8080/groups/create"

  it should "successfully create a group" in {

    (mockGroupService.createGroup _)
      .expects(groupRepr, 1)
      .returns(Future.successful(3))

    Post("/groups/create", groupRepr).withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.OK
    }

  }

  it should "fail if no authHeader provided" in {
    Post("/groups/create", groupRepr) ~> route ~> check {
      status shouldBe StatusCodes.FORBIDDEN
    }

  }

  it should "fail if group with provided name already exists" in {

    (mockGroupService.createGroup _)
      .expects(groupRepr, 1)
      .returns(Future.failed(NameAlreadyExistsException()))

    Post("/groups/create", groupRepr).withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.CONFLICT
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "A group with such name already exists"
      )

    }

  }

  it should "fail if users doesn't exist" in {
    (mockGroupService.createGroup _)
      .expects(groupRepr, 10)
      .returns(Future.failed(UserNotFoundException()))
    Post("/groups/create", groupRepr).withHeaders(
      Seq(authHeader(10))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NOT_FOUND
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }

  }

  private val mockGroupService = mock[GroupServiceImpl]
  private val mockAuthService = mock[AuthServiceImpl]
  private val authRoute =
    Route.seal((new AuthRoutes(mockAuthService).routes))(exceptionHandler =
      PetMeeter.eh
    )
  private val route =
    Route.seal((new GroupRoutes(mockGroupService).routes))(exceptionHandler =
      PetMeeter.eh
    )
  private def authToken(userId: Long) = {

    (mockAuthService.login _)
      .expects(Credentials("email", "password"))
      .returns(Future.successful(userId))
    val r =
      Post("/login", Credentials("email", "password")) ~> authRoute ~> check {
        header("Set-Authorization") match {
          case Some(v) => v
        }
      }
    r.value()
  }

  private val groupRepr = GroupsRepr(
    None,
    "groupname",
    "groupdescr",
    Some(1),
    Some(Seq("sport, games"))
  )
  private def authHeader(userId: Long) =
    RawHeader("Authorization", authToken(userId))

}
