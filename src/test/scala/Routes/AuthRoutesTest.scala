package Routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import petmeeter.routes.{ExceptionResponse, PetMeeter}
import petmeeter.services.auth.AuthServiceImpl
import petmeeter._

import scala.concurrent.Future

class AuthRoutesTest
    extends SampleRepo
    with AsyncMockFactory
    with ScalatestRouteTest
    with Matchers {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  behavior of "http://localhost:8080/register"

  it should "successfully register a user" in {
    (mockAuthService.register _)
      .expects(cred)
      .returns(Future.successful(true))
    Post("/register", cred) ~> route ~> check {
      status shouldBe StatusCodes.OK
    }
  }

  it should "fail if email is occupied" in {
    (mockAuthService.register _)
      .expects(cred)
      .returns(Future.failed(EmailAlreadyExistsException()))
    Post("/register", cred) ~> route ~> check {
      status shouldBe StatusCodes.Conflict
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "Provided email already exists!"
      )
    }
  }

  it should "fail if provided email is not valid" in {
    (mockAuthService.register _)
      .expects(cred)
      .returns(Future.failed(NotAValidEmailException()))
    Post("/register", cred) ~> route ~> check {
      status shouldBe StatusCodes.BadRequest
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "Provided email is not valid!"
      )
    }
  }

  behavior of "http://localhost:8080/login"

  it should "successfully login a user" in {
    (mockAuthService.login _)
      .expects(cred)
      .returns(Future.successful(1))

    Post("/login", cred) ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[String] shouldBe "You are logged in"

      assert {
        header("Set-Authorization") match {
          case None        => false
          case Some(token) => true
        }
      }
    }
  }

  it should "fail if no wrong credentials provided" in {
    (mockAuthService.login _)
      .expects(cred)
      .returns(Future.failed(WrongCredentialsException()))

    Post("/login", cred) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "Provided credentials do not match existing user"
      )

    }
  }

  private val mockAuthService = mock[AuthServiceImpl]
  private val route = Route.seal(
    new petmeeter.routes.AuthRoutes(mockAuthService).routes
  )(exceptionHandler = PetMeeter.eh)

  private def authToken(userId: Long) = {

    (mockAuthService.login _)
      .expects(Credentials("email", "password"))
      .returns(Future.successful(userId))
    val r = Post("/login", Credentials("email", "password")) ~> route ~> check {
      header("Set-Authorization") match {
        case Some(v) => v
      }
    }
    r.value()
  }

  private def authHeader(userId: Long) =
    RawHeader("Authorization", authToken(userId))

  private val cred = Credentials("email@email.com", "password")

}
