package Routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.matchers.Matchers
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import petmeeter.data.Tables.{Group, Message, Notification, Topic}
import petmeeter.routes.{AuthRoutes, ExceptionResponse, PetMeeter, UserRoutes}
import petmeeter.services.auth.AuthServiceImpl
import petmeeter.services.users.{UserService, UsersRepr}
import petmeeter.{Credentials, GroupNotFoundException, SampleRepo, UserNotFoundException}

import scala.concurrent.Future

class UserRoutesTest
    extends SampleRepo
    with AsyncMockFactory
    with ScalatestRouteTest
    with Matchers {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  behavior of "http://localhost:8080/users/all"

  it should "return a valid list of Users" in {
    (mockUserService.getAllUsers _)
      .expects()
      .returns(Future.successful(Seq(userRepr)))

    Get("/users/all") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[UsersRepr]] shouldBe Seq(userRepr)
    }
  }

  behavior of "http://localhost:8080/users/{userId}"

  it should "successfully return a user" in {
    (mockUserService.getUserById _)
      .expects(1)
      .returns(Future.successful(userRepr))

    Get("/users/1") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[UsersRepr] shouldBe userRepr
    }
  }

  it should "fail with UserNotFoundException if user with such Id exists" in {
    (mockUserService.getUserById _)
      .expects(10)
      .returns(Future.failed(UserNotFoundException()))

    Get("/users/10") ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe (ExceptionResponse(
        "No user exists matching provided data!"
      ))
    }
  }

  behavior of "http://localhost:8080/users/{userId}/topics"

  it should "successfully return users topics" in {
    (mockUserService.getUserTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    Get("/users/1/topics") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Topic]] shouldBe SampleTopics.take(2)
    }
  }

  it should "successfully return an empty list if user has no topics" in {
    (mockUserService.getUserTopicsById _)
      .expects(1)
      .returns(Future.successful(Seq()))

    Get("/users/1/topics") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Topic]] shouldBe Seq()
    }
  }

  it should "fail with UserNotFoundException if no user with such id" in {
    (mockUserService.getUserTopicsById _)
      .expects(10)
      .returns(Future.failed(UserNotFoundException()))

    Get("/users/10/topics") ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }
  }

  behavior of "http://localhost:8080/users/{userId}/groups"

  it should "successfully return users groups" in {
    (mockUserService.getUserGroupsById _)
      .expects(1)
      .returns(Future.successful(SampleGroups.take(2)))

    Get("/users/1/groups") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Group]] shouldBe SampleGroups.take(2)
    }
  }

  it should "successfully return an empty list if user has no groups" in {
    (mockUserService.getUserGroupsById _)
      .expects(1)
      .returns(Future.successful(Seq()))

    Get("/users/1/groups") ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Group]] shouldBe Seq()
    }
  }

  it should "fail with UserNotFoundException if no user with such id" in {
    (mockUserService.getUserGroupsById _)
      .expects(10)
      .returns(Future.failed(UserNotFoundException()))

    Get("/users/10/groups") ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }
  }

  behavior of "http://localhost:8080/users/{userId}/joinGroup?groupId={groupId}"

  it should "successfully join a group" in {

    (mockUserService.joinGroup _)
      .expects(1, 2)
      .returns(Future.successful(true))

    Post("/users/1/joinGroup?groupId=2").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Boolean] shouldBe true
    }
  }

  it should "fail if no AuthHeader provided " in {
    Post("/users/1/joinGroup?groupId=2") ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  it should "fail if a wrong auth token provided" in {
    Post("/users/2/joinGroup?groupId=2").withHeaders(
      Seq(authHeader(3))
    ) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "You are not authorized for this action"
      )
    }
  }

  it should "fail if a user doesn't exist" in {
    (mockUserService.joinGroup _)
      .expects(10, 2)
      .returns(Future.failed(UserNotFoundException()))

    Post("/users/10/joinGroup?groupId=2").withHeaders(
      Seq(authHeader(10))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }

  }

  it should "fail if a group doesn't exist" in {
    (mockUserService.joinGroup _)
      .expects(1, 10)
      .returns(Future.failed(GroupNotFoundException()))

    Post("/users/1/joinGroup?groupId=10").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No group exists matching provided data!"
      )
    }

  }

  behavior of "http://localhost:8080/users/{userId}/leaveGroup?groupId={groupId}"

  it should "successfully leave a group" in {

    (mockUserService.leaveGroup _)
      .expects(1, 2)
      .returns(Future.successful(true))

    Post("/users/1/leaveGroup?groupId=2").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Boolean] shouldBe true
    }
  }

  it should "fail if no AuthHeader provided " in {
    Post("/users/1/leaveGroup?groupId=2") ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  it should "fail if a wrong auth token provided" in {
    Post("/users/2/leaveGroup?groupId=2").withHeaders(
      Seq(authHeader(3))
    ) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "You are not authorized for this action"
      )
    }
  }

  it should "fail if a user doesn't exist" in {
    (mockUserService.leaveGroup _)
      .expects(10, 2)
      .returns(Future.failed(UserNotFoundException()))

    Post("/users/10/leaveGroup?groupId=2").withHeaders(
      Seq(authHeader(10))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }

  }

  it should "fail if a group doesn't exist" in {
    (mockUserService.leaveGroup _)
      .expects(1, 10)
      .returns(Future.failed(GroupNotFoundException()))

    Post("/users/1/leaveGroup?groupId=10").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No group exists matching provided data!"
      )
    }

  }

  behavior of "http://localhost:8080/users/{userId}/updateTopics"

  it should "Successfully update topics" in {

    (mockUserService.updateUserTopics _)
      .expects(Seq("new topic"), 1)
      .returns(Future.successful(Seq(Topic(1, "new topic"))))

    Post("/users/1/updateTopics", Seq("new topic"))
      .withHeaders(Seq(authHeader(1))) ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[Seq[Topic]] shouldBe Seq(Topic(1, "new topic"))

    }
  }

  it should "fail if no auth token provided" in {
    Post("/users/1/updateTopics", Seq("new topic")) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  it should "fail if wrong auth token provided" in {

    Post("/users/1/updateTopics", Seq("new topic"))
      .withHeaders(Seq(authHeader(10))) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "You are not authorized for this action"
      )
    }
  }

  it should "fail if user not found" in {

    (mockUserService.updateUserTopics _)
      .expects(Seq("new topic"), 10)
      .returns(Future.failed(UserNotFoundException()))

    Post("/users/10/updateTopics", Seq("new topic"))
      .withHeaders(Seq(authHeader(10))) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }
  }

  behavior of "http://localhost:8080/users/{userId}/update"

  it should "successfully update a user" in {
    (mockUserService.updateUser _)
      .expects(userRepr, 1)
      .returns(Future.successful(userRepr))

    Post("/users/1/update", userRepr).withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.OK
      responseAs[UsersRepr] shouldBe userRepr
    }
  }

  it should "fail if no auth header provided" in {
    Post("/users/1/update", userRepr) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
    }
  }

  it should "fail if wrong auth header provided" in {
    Post("/users/1/update", userRepr).withHeaders(
      Seq(authHeader(10))
    ) ~> route ~> check {
      status shouldBe StatusCodes.Forbidden
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "You are not authorized for this action"
      )
    }
  }

  it should "fail if no such user exists" in {
    (mockUserService.updateUser _)
      .expects(userRepr, 10)
      .returns(Future.failed(UserNotFoundException()))

    Post("/users/10/update", userRepr).withHeaders(
      Seq(authHeader(10))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }
  }

  behavior of "http://localhost:8080/users/{userId}/messages"

  it should "successfully return user's messages" in {
    (mockUserService.getAllUserMessages _)
      .expects(1)
      .returns(Future.successful(SampleMessages.take(2)))

    Get("/users/1/messages").withHeaders(Seq(authHeader(1))) ~> route ~> check {

      status shouldBe StatusCodes.OK
      responseAs[Seq[Message]] shouldBe SampleMessages.take(2)
    }

  }

  it should "fail if no authHeader proivded" in {
    Get("/users/1/messages") ~> route ~> check {

      status shouldBe StatusCodes.Forbidden
    }

  }

  it should "fail if user doesn't exist" in {
    (mockUserService.getAllUserMessages _)
      .expects(1)
      .returns(Future.failed(UserNotFoundException()))

    Get("/users/1/messages").withHeaders(Seq(authHeader(1))) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }

  }

  behavior of "http://localhost:8080/users/{userId}/notifications"

  it should "successfully return user's notifications" in {
    (mockUserService.getUserNotifications _)
      .expects(1)
      .returns(Future.successful(SampleNotifications.take(2)))

    Get("/users/1/notifications").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {

      status shouldBe StatusCodes.OK
      responseAs[Seq[Notification]] shouldBe SampleNotifications.take(2)
    }

  }

  it should "fail if no authHeader proivded" in {
    Get("/users/1/notifications") ~> route ~> check {

      status shouldBe StatusCodes.Forbidden
    }

  }

  it should "fail if user doesn't exist" in {
    (mockUserService.getUserNotifications _)
      .expects(1)
      .returns(Future.failed(UserNotFoundException()))

    Get("/users/1/notifications").withHeaders(
      Seq(authHeader(1))
    ) ~> route ~> check {
      status shouldBe StatusCodes.NotFound
      responseAs[ExceptionResponse] shouldBe ExceptionResponse(
        "No user exists matching provided data!"
      )
    }

  }

  private val mockUserService = mock[UserService]
  private val mockAuthService = mock[AuthServiceImpl]
  private val authRoute =
    Route.seal((new AuthRoutes(mockAuthService).routes))(exceptionHandler =
      PetMeeter.eh
    )
  private val route =
    Route.seal((new UserRoutes(mockUserService).routes))(exceptionHandler =
      PetMeeter.eh
    )
  private val userRepr = UsersRepr(
    Some(1),
    Some(SampleUsers(0).email),
    SampleUsers(0).firstname,
    SampleUsers(0).lastname,
    Some(SampleTopics.take(2)),
    Some(SampleGroups.take(2))
  )

  private def authToken(userId: Long) = {

    (mockAuthService.login _)
      .expects(Credentials("email", "password"))
      .returns(Future.successful(userId))
    val r =
      Post("/login", Credentials("email", "password")) ~> authRoute ~> check {
        header("Set-Authorization") match {
          case Some(v) => v
        }
      }
    r.value()
  }

  private def authHeader(userId: Long) =
    RawHeader("Authorization", authToken(userId))

}
