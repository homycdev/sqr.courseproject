package Service

import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import petmeeter.SampleRepo
import petmeeter.data.DataHandler
import petmeeter.services.users.{UserServiceImpl, UsersRepr}

import scala.concurrent.Future

class UserServiceTest extends SampleRepo with AsyncMockFactory {

  behavior of "getAllUsers"

  it should "Successfully return a user converted to UsersRepr" in {
    (mockUserHandler.getAllUsers _)
      .expects()
      .returns(Future.successful(SampleUsers.take(1)))

    (mockUserHandler.getUserTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    (mockUserHandler.getUserGroupsById _)
      .expects(1)
      .returns(Future.successful(SampleGroups.take(2)))

    for {
      res <- service.getAllUsers()
    } yield res shouldBe Seq(userRepr)

  }

  behavior of "updateUser"

  it should "Successfully update a user" in {

    (mockUserHandler.updateUser _)
      .expects(SampleUsers(0).copy(firstname = Some("ChangedName")))
      .returns(
        Future.successful(SampleUsers(0).copy(firstname = Some("ChangedName")))
      )

    (mockUserHandler.getUserById _)
      .expects(1)
      .returns(Future.successful(SampleUsers(0)))

    (mockUserHandler.getUserTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    (mockUserHandler.getUserGroupsById _)
      .expects(1)
      .returns(Future.successful(SampleGroups.take(2)))

    for {
      res <-
        service.updateUser(userRepr.copy(firstname = Some("ChangedName")), 1)
    } yield res shouldBe userRepr.copy(firstname = Some("ChangedName"))

  }
  it should "Successfully update a user given a single valid parameter" in {
    val nameOnly =
      UsersRepr(None, None, Some("Only name is changed"), None, None, None)

    (mockUserHandler.updateUser _)
      .expects(SampleUsers(0).copy(firstname = Some("Only name is changed")))
      .returns(
        Future.successful(
          SampleUsers(0).copy(firstname = Some("Only name is changed"))
        )
      )

    (mockUserHandler.getUserById _)
      .expects(1)
      .returns(Future.successful(SampleUsers(0)))

    (mockUserHandler.getUserTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    (mockUserHandler.getUserGroupsById _)
      .expects(1)
      .returns(Future.successful(SampleGroups.take(2)))

    for {
      res <- service.updateUser(nameOnly, 1)
    } yield res shouldBe userRepr.copy(firstname = Some("Only name is changed"))

  }

  behavior of "getUserById"

  it should "Successfully return a user" in {

    (mockUserHandler.getUserById _)
      .expects(1)
      .returns(Future.successful(SampleUsers(0)))

    (mockUserHandler.getUserTopicsById _)
      .expects(1)
      .returns(Future.successful(SampleTopics.take(2)))

    (mockUserHandler.getUserGroupsById _)
      .expects(1)
      .returns(Future.successful(SampleGroups.take(2)))

    for {
      res <- service.getUserById(1)
    } yield res shouldBe userRepr

  }

  // ------------------ INTERNAL ---------------------
  private val mockUserHandler: DataHandler = mock[DataHandler]
  private val service = new UserServiceImpl(mockUserHandler)
  private val userRepr = UsersRepr(
    Some(1),
    Some(SampleUsers(0).email),
    SampleUsers(0).firstname,
    SampleUsers(0).lastname,
    Some(SampleTopics.take(2)),
    Some(SampleGroups.take(2))
  )
}
