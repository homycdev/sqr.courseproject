package Service

import com.github.t3hnar.bcrypt._
import org.scalamock.scalatest.AsyncMockFactory
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import petmeeter.data.DataHandler
import petmeeter.data.Tables.User
import petmeeter.services.auth.AuthServiceImpl
import petmeeter.{Credentials, NotAValidEmailException, WrongCredentialsException}

import scala.concurrent.Future

class AuthServiceTest
    extends AsyncFlatSpec
    with AsyncMockFactory
    with Matchers {

  behavior of "register"

  it should "successfully register a User" in {
    (mockUserHandler.createUser _)
      .expects(credentials.email, *)
      .returns(Future.successful(1))

    for {
      result <- service.register(credentials)
    } yield result shouldBe true

  }

  it should "fail with NotAValidEmailException if provided email is not valid" in {

    recoverToSucceededIf[NotAValidEmailException] {

      for {
        _ <- service.register(credentials.copy(email = "not a valid email"))
      } yield ()
    }

  }

  behavior of "login"

  it should "successfully login a User" in {

    (mockUserHandler.getUserByEmail _)
      .expects(credentials.email)
      .returns(
        Future.successful(
          User(1, Some("Name"), Some("Last Name"), credentials.email, hash)
        )
      )

    for {
      result <- service.login(credentials)
    } yield result shouldBe (1)
  }

  it should "fail on invalid credentials" in {
    recoverToSucceededIf[WrongCredentialsException] {
      (mockUserHandler.getUserByEmail _)
        .expects(credentials.email)
        .returns(
          Future.successful(
            User(1, Some("Name"), Some("Last Name"), credentials.email, hash)
          )
        )

      for {
        result <- service.login(credentials.copy(password = "invalid password"))
      } yield result shouldBe (1)
    }
  }

  private val mockUserHandler: DataHandler = mock[DataHandler]
  private val service = new AuthServiceImpl(mockUserHandler)
  private val credentials = Credentials("email@gmail.com", "password")
  private val hash = "password".bcryptBounded(12)

}
