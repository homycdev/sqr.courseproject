package petmeeter
import org.scalatest.flatspec.AsyncFlatSpec
import petmeeter.data.Tables._

class SampleRepo extends AsyncFlatSpec {
  protected val SampleUsers = Seq(
    User(1, Some("Name1"), Some("Surname1"), "email1@gmail.com", "hash1"),
    User(2, Some("Name2"), Some("Surname2"), "email2@gmail.com", "hash2"),
    User(3, Some("Name3"), Some("Surname3"), "email3@gmail.com", "hash3")
  )

  protected val SampleGroups = Seq(
    Group(1, "GroupName1", "GroupDesc1", 1),
    Group(2, "GroupName2", "GroupDesc2", 1),
    Group(3, "GroupName3", "GroupDesc3", 2),
    Group(4, "GroupName4", "GroupDesc4", 2),
    Group(5, "GroupName5", "GroupDesc5", 1)
  )

  protected val SampleUserGroups = Seq(
    Usergroup(1, 1),
    Usergroup(1, 2),
    Usergroup(1, 3),
    Usergroup(2, 5),
    Usergroup(2, 4)
  )

  protected val SampleTopics = Seq(
    Topic(1, "sport"),
    Topic(2, "coding"),
    Topic(3, "games"),
    Topic(4, "cooking")
  )

  protected val SampleUserTags = Seq(
    Usertopic(1, 1),
    Usertopic(1, 2),
    Usertopic(1, 3),
    Usertopic(2, 3),
    Usertopic(2, 4)
  )

  protected val SampleGroupTags = Seq(
    Grouptopic(1, 1),
    Grouptopic(1, 2),
    Grouptopic(1, 3),
    Grouptopic(2, 1)
  )

  protected val SampleComments = Seq(
    Comment(1, 1, 1, "Text1", None),
    Comment(2, 1, 2, "Text2", None),
    Comment(3, 1, 3, "Text3", None),
    Comment(4, 2, 3, "Text1", None),
    Comment(5, 2, 2, "Text1", None)
  )

  protected val SampleMessages = Seq(
    Message(1, 1, 2, "Text1", None),
    Message(2, 1, 3, "Text1", None),
    Message(3, 2, 3, "Text1", None)
  )

  protected val SampleNotifications = Seq(
    Notification(1, 1, 1, "Notif1", None),
    Notification(2, 2, 1, "Notif1", None),
    Notification(3, 1, 2, "Notif1", None)
  )

}
