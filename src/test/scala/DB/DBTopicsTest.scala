package DB

import petmeeter.data.Tables.Topic
import petmeeter.{GroupNotFoundException, UserNotFoundException}

class DBTopicsTest extends DBSuite {

  behavior of "getUserTagsById"

  it should "successfully get users topics" in {
    for {
      db <- test()
      result <- h(db)
        .getUserTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics.take(3)
  }

  it should "return an empty list if user has no topics" in {
    for {
      db <- test()
      result <- h(db)
        .getUserTopicsById(3)
        .andThen { case _ => db.close() }
    } yield result shouldBe Seq()
  }

  it should "fail with UserNotFoundException if no user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        _ <- h(db)
          .getUserTopicsById(10)
          .andThen { case _ => db.close() }
      } yield ()
    }
  }

  behavior of "updateUserTags"

  it should "successfully update topics" in {
    for {
      db <- test()
      _ <- h(db).updateUserTopics(SampleTopics.map(_.name), 1)
      result <- h(db)
        .getUserTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics
  }

  it should "not update topics if provided topics match users topics" in {
    for {
      db <- test()
      _ <- h(db).updateUserTopics(SampleTopics.take(3).map(_.name), 1)
      result <- h(db)
        .getUserTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics.take(3)
  }

  it should "not remove topics if provided topics < usertopics" in {
    for {
      db <- test()
      _ <- h(db).updateUserTopics(SampleTopics.take(2).map(_.name), 1)
      result <- h(db)
        .getUserTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics.take(2)
  }

  it should "update user topics, creating a new topic" in {
    val newTag = Topic(0, "newTag")
    for {
      db <- test()
      _ <- h(db).updateUserTopics(SampleTopics.appended(newTag).map(_.name), 1)
      result <- h(db)
        .getUserTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result
      .map(_.name) shouldBe SampleTopics.appended(newTag).map(_.name)

  }

  it should "be able to delete topics, add topics from existing list and also add new topics " in {
    val newTag = Topic(0, "newTag")
    for {
      db <- test()
      _ <- h(db)
        .updateUserTopics(SampleTopics.drop(1).appended(newTag).map(_.name), 1)
      result <- h(db)
        .getUserTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result
      .map(_.name) shouldBe SampleTopics.drop(1).appended(newTag).map(_.name)
  }

  it should "fail with UserNotFoundException if no such user exists" in {
    recoverToSucceededIf[UserNotFoundException] {
      for {
        db <- test()
        _ <- h(db)
          .updateUserTopics(SampleTopics.map(_.name), 10)
          .andThen { case _ => db.close() }
      } yield ()
    }
  }

  behavior of "getGroupTagsById"

  it should "get sequence of topics" in {
    for {
      db <- test()
      result <- h(db)
        .getGroupTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics.take(3)
  }

  it should "get empty sequence if group has no topics" in {
    for {
      db <- test()
      result <- h(db)
        .getGroupTopicsById(3)
        .andThen { case _ => db.close() }
    } yield result shouldBe Seq()
  }

  it should "fail with GroupNotFoundException" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        _ <- h(db)
          .getGroupTopicsById(10)
          .andThen { case _ => db.close() }
      } yield ()
    }
  }

  behavior of "getUsersByTopics"

  it should "Return list of users" in {
    for {
      db <- test()
      result <- h(db)
        .getUsersByTopics(Seq("games"))
        .andThen { case _ => db.close() }
    } yield result should contain theSameElementsAs Seq(1, 2)

  }

  it should "Return empty list if no user has such topic" in {
    for {
      db <- test()
      result <- h(db)
        .getUsersByTopics(Seq("nonExistantTopic"))
        .andThen { case _ => db.close() }
    } yield result shouldBe Seq()

  }

  it should "return a valid list of users even if some topics are nonexistant" in {
    for {
      db <- test()
      result <- h(db)
        .getUsersByTopics(SampleTopics.map(_.name).appended("NonExistantTopic"))
        .andThen { case _ => db.close() }
    } yield result shouldBe Seq(1, 2)

  }

  behavior of "getGroupsByTopics"

  it should "return a valid sequence of groups" in {
    for {
      db <- test()
      result <- h(db)
        .getGroupsByTopics(Seq("sport"))
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleGroups.take(2)
  }

  it should "return an empty sequence if nothing matches" in {
    for {
      db <- test()
      result <- h(db)
        .getGroupsByTopics(SampleTopics.map(_.name))
        .andThen { case _ => db.close() }
    } yield result shouldBe Seq()
  }

  behavior of "updateGroupTags"

  it should "successfully update topics" in {
    for {
      db <- test()
      _ <- h(db).updateGroupTopics(SampleTopics.map(_.name), 1)
      result <- h(db)
        .getGroupTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics
  }

  it should "not update topics if provided topics match users topics" in {
    for {
      db <- test()
      _ <- h(db).updateGroupTopics(SampleTopics.take(3).map(_.name), 1)
      result <- h(db)
        .getGroupTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics.take(3)
  }

  it should "remove topics if provided topics < usertopics" in {
    for {
      db <- test()
      _ <- h(db).updateGroupTopics(SampleTopics.take(2).map(_.name), 1)
      result <- h(db)
        .getGroupTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result shouldBe SampleTopics.take(2)
  }

  it should "update group topics, creating a new topic" in {
    val newTag = Topic(0, "newTag")
    for {
      db <- test()
      _ <- h(db).updateGroupTopics(SampleTopics.appended(newTag).map(_.name), 1)
      result <- h(db)
        .getGroupTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result
      .map(_.name) shouldBe SampleTopics.appended(newTag).map(_.name)

  }

  it should "be able to delete topics, add topics from existing list and also add new topics " in {
    val newTag = Topic(0, "newTag")
    for {
      db <- test()
      _ <- h(db)
        .updateGroupTopics(SampleTopics.drop(1).appended(newTag).map(_.name), 1)
      result <- h(db)
        .getGroupTopicsById(1)
        .andThen { case _ => db.close() }
    } yield result
      .map(_.name) shouldBe SampleTopics.drop(1).appended(newTag).map(_.name)
  }

  it should "fail with Group NotFoundException if no such user exists" in {
    recoverToSucceededIf[GroupNotFoundException] {
      for {
        db <- test()
        _ <- h(db)
          .updateGroupTopics(SampleTopics.map(_.name), 10)
          .andThen { case _ => db.close() }
      } yield ()
    }
  }

}
