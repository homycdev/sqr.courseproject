package petmeeter

import slick.codegen.SourceCodeGenerator
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile.{createModelBuilder, defaultTables}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
// DO NOT RUN!!!!
object generateSlickBoilerplate {

  val DBCONFIGNAME = "petmeeter"

  def makeCaseClassName(name: String): String = {
    (name).toLowerCase.capitalize
  }

  def generateCompanionObject(name: String): String = {
    "\n\n" +
      "object " + makeCaseClassName(name) + " { \n" +
      "// This definition is required for proper \n" +
      "// slick functionality with companion objects \n" +
      "val tupled = (this.apply _).tupled \n" +
      s"\timplicit val jsonDecoder: Decoder[${makeCaseClassName(name)}] = deriveDecoder\n" +
      s"\timplicit val jsonEncoder: Encoder[${makeCaseClassName(name)}] = deriveEncoder\n" +
      "} \n"
  }

  def main(args: Array[String]): Unit = {
    Await.ready(
      codegen.map(
        _.writeToFile(
          "slick.jdbc.PostgresProfile",
          "src\\main\\scala",
          "petmeeter.Data"
        )
      ),
      20.seconds
    )

  }

  private val db = Database.forConfig(DBCONFIGNAME)
  private val codegen = db.run(
    defaultTables
      .flatMap(createModelBuilder(_, ignoreInvalidDefaults = false).buildModel)
      .map { model =>
        new SourceCodeGenerator(model) {
          // Generated case classes names are capitalized
          override def entityName: String => String = makeCaseClassName(_)

          // Generating table class names in a certain way
          override def tableName: String => String =
            makeCaseClassName(_).concat("Table")

          override def Table =
            new Table(_) {
              table =>
              val tableName: String = table.model.name.table

              // Generating companion object for every entity Case Class

              override def EntityType: AnyRef with EntityTypeDef =
                new EntityTypeDef {

                  override def code: String =
                    super.code + generateCompanionObject(tableName)
                }

              // This autogen converts postgres SERIAL into Int by default. This
              // changes it to Long
              override def Column =
                new Column(_) {
                  override def rawType: String = {
                    if (model.name.contains("id")) "Long"
                    else if (model.name.contains("timeat")) "java.time.Instant"
                    else super.rawType
                  }

                }

            }

          // Importing dependencies required for JSON conversion
          override def code: String =
            "import io.circe._, io.circe.generic.semiauto._ \n" + super.code

          // Disable ddl generation
          override val ddlEnabled = false
        }
      }
  )
}
