package petmeeter

sealed abstract class PetMeeter(message: String)
    extends Exception(message)

class NotFoundException(text: String) extends PetMeeter(text)

final case class UserNotFoundException()
    extends NotFoundException("No user exists matching provided data!")

final case class GroupNotFoundException()
    extends NotFoundException("No group exists matching provided data!")

class AlreadyExistsException(text: String) extends PetMeeter(text)

final case class NameAlreadyExistsException()
    extends AlreadyExistsException("A group with such name already exists")

final case class EmailAlreadyExistsException()
    extends AlreadyExistsException("Provided email already exists!")

class RestrictedException(text: String) extends PetMeeter(text)

final case class NotAuthorizedException()
    extends RestrictedException("You are not authorized for this action")

final case class WrongCredentialsException()
    extends RestrictedException(
      "Provided credentials do not match existing user"
    )

class NotAValidException(text: String) extends PetMeeter(text)

final case class NotAValidEmailException()
    extends NotAValidException("Provided email is not valid!")

final case class NotAValidMeetingExeption()
    extends NotAValidException("Provided meeting data is not valid")
