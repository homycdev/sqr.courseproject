package petmeeter

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.cors
import petmeeter.data.SlickDataHandler
import petmeeter.routes.{AuthRoutes, GroupRoutes, PetMeeter, UserRoutes}
import petmeeter.services.auth.AuthServiceImpl
import petmeeter.services.groups.GroupServiceImpl
import petmeeter.services.users.UserServiceImpl

import scala.concurrent.ExecutionContextExecutor

object petmeeterHttp extends App {

  import slick.jdbc.JdbcBackend.Database

  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContextExecutor = ac.dispatcher

  //Using PostgreSQL
  val db = Database.forConfig("petmeeter")

  // Handles data
  val dataHandler = new SlickDataHandler(db)
  // Handles exceptions
  val exceptionHandler = PetMeeter

  // Services
  val authService = new AuthServiceImpl(dataHandler)
  val userService = new UserServiceImpl(dataHandler)
  val groupService = new GroupServiceImpl(dataHandler)

  //Routes
  val authRoutes = new AuthRoutes(authService).routes
  val userRoutes = new UserRoutes(userService).routes
  val groupRoutes = new GroupRoutes(groupService).routes

  val route = authRoutes ~ userRoutes ~ groupRoutes

  val routes: Route = Route.seal(route)(exceptionHandler = exceptionHandler.eh)

  Http()
    .newServerAt("0.0.0.0", 80)
    .bind(cors() {
      routes
    })
}
