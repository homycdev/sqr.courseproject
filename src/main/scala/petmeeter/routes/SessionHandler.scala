package petmeeter.routes

import akka.http.scaladsl.server.{Directive0, Directive1}
import com.softwaremill.session.SessionDirectives.{invalidateSession, requiredSession, setSession}
import com.softwaremill.session.SessionOptions.{oneOff, usingHeaders}
import com.softwaremill.session.{SessionConfig, SessionManager}
import petmeeter.NotAuthorizedException

import scala.concurrent.{ExecutionContext, Future}

class SessionHandler(implicit ec: ExecutionContext) {

  private val sessionConfig = SessionConfig.default(
    "tQhZlAoYQpIgjxHSWeluRXQon18xFeHvZZ1wzzQAEZFz0c15a3b3ZIWEwFUDm6FB"
  )
  implicit val sessionManager: SessionManager[Long] =
    new SessionManager[Long](sessionConfig)

  def provideSession(value: Future[Long]): Future[Directive0] =
    value.map(setSession(oneOff, usingHeaders, _))

  def verify(session: Long, userId: Long): Future[Boolean] = {
    if (session == userId) {
      Future.successful(true)
    } else {
      Future.failed(NotAuthorizedException())
    }
  }

  val withSession: Directive1[Long] = requiredSession(oneOff, usingHeaders)
  // Used for invalidating cookies
  val myInvalidateSession: Directive0 = invalidateSession(oneOff, usingHeaders)

}
