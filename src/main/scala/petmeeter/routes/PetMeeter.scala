package petmeeter.routes

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler
import petmeeter._

object PetMeeter {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val eh: ExceptionHandler =
    ExceptionHandler {
      case e: NotFoundException =>
        complete(NotFound, ExceptionResponse(e.getMessage))
      case e: AlreadyExistsException =>
        complete(Conflict, ExceptionResponse(e.getMessage))
      case e: RestrictedException =>
        complete(Forbidden, ExceptionResponse(e.getMessage))
      case e: NotAValidException =>
        complete(BadRequest, ExceptionResponse(e.getMessage))
      case e: PetMeeter =>
        complete(BadRequest, ExceptionResponse(e.getMessage))
    }
}
