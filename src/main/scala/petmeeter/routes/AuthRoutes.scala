package petmeeter.routes

import akka.http.scaladsl.server.Route
import petmeeter.Credentials
import petmeeter.services.auth.AuthService

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

class AuthRoutes(service: AuthService)(implicit ec: ExecutionContext) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val sh = new SessionHandler()

  private val registerRoute: Route = path("register") {
    post {
      entity(as[Credentials]) { c =>
        complete(service.register(c))
      }
    }
  }

  private val loginRoute: Route = path("login") {

    post {
      entity(as[Credentials]) { credentials =>
        onComplete(sh.provideSession(service.login(credentials))) {
          case Success(dir) =>
            dir {
              complete("You are logged in")
            }
          case Failure(ex) => complete(ex)
        }
      }
    }
  }

  val routes: Route = concat(registerRoute, loginRoute)

}
