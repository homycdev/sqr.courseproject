package petmeeter.services.auth

import petmeeter.Credentials

import scala.concurrent.Future

trait AuthService {

  def register(credentials: Credentials): Future[Boolean]

  def login(credentials: Credentials): Future[Long]

}
