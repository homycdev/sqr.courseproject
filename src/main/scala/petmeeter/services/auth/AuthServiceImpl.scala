package petmeeter.services.auth

import petmeeter.data.DataHandler
import petmeeter.services.bcryptAsync
import petmeeter.{Credentials, NotAValidEmailException, WrongCredentialsException}

import scala.concurrent.{ExecutionContext, Future}

class AuthServiceImpl(handler: DataHandler)(implicit ec: ExecutionContext)
    extends AuthService {

  override def login(credentials: Credentials): Future[Long] = {

    def patternMatch(verified: Boolean) =
      if (verified) {
        Future.successful(true)
      } else {
        Future.failed(WrongCredentialsException())
      }

    for {
      user <- handler.getUserByEmail(credentials.email)
      verified <- bcryptAsync.verify(credentials.password, user.hashed)
      _ <- patternMatch(verified)
    } yield user.id

  }

  override def register(credentials: Credentials): Future[Boolean] = {

    (if (isValidEmail(credentials.email)) {
       Future.successful(true)
     } else {
       Future.failed(NotAValidEmailException())
     })
      .flatMap(_ => bcryptAsync.hash(credentials.password))
      .flatMap { hashed => handler.createUser(credentials.email, hashed) }
      .flatMap { _ => Future.successful(true) }
  }

  //----------------------------INTERNAL-------------------------------

  private def isValidEmail(email: String): Boolean = {
    val regex =
      "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])".r
    regex.findFirstIn(email) match {
      case None    => false
      case Some(_) => true
    }
  }

}
