package petmeeter.services

import com.github.t3hnar.bcrypt._

import scala.concurrent.{ExecutionContext, Future}

object bcryptAsync {

  private val rounds: Int = 12

  def hash(password: String)(implicit ec: ExecutionContext): Future[String] = {
    Future(password.bcryptBounded(rounds))
  }

  def verify(password: String, hash: String)(implicit
      ec: ExecutionContext
  ): Future[Boolean] = {
    Future(password.isBcryptedBounded(hash))
  }

}
