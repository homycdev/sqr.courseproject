package petmeeter.services.users

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}
import petmeeter.data.Tables._

// We are using this case class as a proxy between Users Database Entity and API to ensure control over sensitive data (not giving away hashed passwords for instance)
case class UsersRepr(
    id: Option[Long],
    email: Option[String],
    firstname: Option[String],
    lastname: Option[String],
    topics: Option[Seq[Topic]],
    groups: Option[Seq[Group]]
)

object UsersRepr {
  implicit val jsonDecoder: Decoder[UsersRepr] = deriveDecoder
  implicit val jsonEncoder: Encoder[UsersRepr] = deriveEncoder
}
