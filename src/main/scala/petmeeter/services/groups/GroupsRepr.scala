package petmeeter.services.groups

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

// We are using this case class as a proxy between Users Database Entity and API to ensure control over sensitive data (not giving away hashed passwords for instance)
case class GroupsRepr(
    id: Option[Long],
    name: String,
    description: String,
    founderId: Option[Long],
    topics: Option[Seq[String]]
)

object GroupsRepr {
  implicit val jsonDecoder: Decoder[GroupsRepr] = deriveDecoder
  implicit val jsonEncoder: Encoder[GroupsRepr] = deriveEncoder
}
