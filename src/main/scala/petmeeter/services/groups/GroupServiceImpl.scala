package petmeeter.services.groups

import petmeeter.data.Tables.{Comment, Group, Meeting, Notification, Topic}
import petmeeter.data.{DataHandler, Tables}
import petmeeter.{NotAValidMeetingExeption, NotAuthorizedException}

import java.time.Instant
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class GroupServiceImpl(handler: DataHandler)(implicit ec: ExecutionContext)
    extends GroupService {

  override def getAllGroups: Future[Seq[GroupsRepr]] = {
    for {
      groups <- handler.getAllGroups()
      res <- foldToGropupsRepr(groups)
    } yield res
  }

  override def getGroupById(id: Long): Future[GroupsRepr] = {
    handler.getGroupById(id).flatMap(mapFromGroup(_))
  }

  override def findGroupsByName(query: String): Future[Seq[GroupsRepr]] = {

    for {
      groups <- handler.findGroupsByPattern(query)
      result <- foldToGropupsRepr(groups)
    } yield result
  }

  override def getGroupTopicsById(id: Long): Future[Seq[Topic]] = {
    handler.getGroupTopicsById(id: Long)
  }

  override def createGroup(
      groupRepr: GroupsRepr,
      founderId: Long
  ): Future[Long] = {

    def createTopics(groupId: Long) =
      groupRepr.topics match {
        case Some(topics) => handler.updateGroupTopics(topics, groupId)
        case None         => Future.successful()
      }

    for {
      group <- mapToGroup(groupRepr, founderId)
      groupId <- handler.createGroup(group, founderId)
      _ <- createTopics(groupId)
      _ <- SendNotificationOnGroup(groupId, groupRepr.topics)
    } yield groupId

  }

  override def findGroupsByTopics(
      topics: Seq[String]
  ): Future[Seq[GroupsRepr]] = {

    for {
      groups <- handler.getGroupsByTopics(topics)
      result <- foldToGropupsRepr(groups)
    } yield result

  }

  override def getAllComments(groupId: Long): Future[Seq[Comment]] = {
    handler.getAllComments(groupId)
  }

  override def addComment(
      text: String,
      groupId: Long,
      userId: Long
  ): Future[Boolean] = {
    val c = Comment(0, groupId, userId, text, Some(Instant.now()))
    handler.addComment(c)
  }

  override def createMeeting(
      text: String,
      userId: Long,
      groupId: Long,
      timeString: String
  ): Future[Boolean] = {

    val parseTime = Future {
      Instant.parse(timeString)
    }.transformWith {
      case Success(value)     => Future.successful(value)
      case Failure(exception) => Future.failed(NotAValidMeetingExeption())
    }

    val ifUserIsGroupCreator =
      handler.getGroupById(groupId).map(_.founderid).flatMap {
        case id: Long if id == userId => Future.successful(true)
        case _                        => Future.failed(NotAuthorizedException())
      }

    def ifTimeIsValid(t: Instant) =
      t.compareTo(Instant.now()) match {
        case diff: Int if diff > 0 => Future.successful(true)
        case _                     => Future.failed(NotAValidMeetingExeption())
      }

    for {
      time <- parseTime
      _ <- ifUserIsGroupCreator
      _ <- ifTimeIsValid(time)
      res <- handler.createMeeting(Meeting(0, groupId, text, Some(time)))
      _ <- sendNotificationsOnMeeting(groupId, time)
    } yield res
  }

  // ---------------------- INTERNAL--------------------------------

  private def mapFromGroup(group: Tables.Group): Future[GroupsRepr] = {

    val groupTopics = getGroupTopicsById(group.id)
    groupTopics.map { topics =>
      GroupsRepr(
        Some(group.id),
        group.groupname,
        group.groupdesc,
        Some(group.founderid),
        Some(topics.map(_.name))
      )
    }

  }

  private def mapToGroup(groupRepr: GroupsRepr, userId: Long): Future[Group] = {

    Future.successful(Group(0, groupRepr.name, groupRepr.description, userId))

  }

  private def sendNotifications(
      groupId: Long,
      userIds: Seq[Long],
      notif: String
  ) = {

    def sendNotification(userId: Long, groupId: Long, text: String) = {
      val notif = Notification(0, groupId, userId, text, Some(Instant.now()))
      handler.sendNotification(notif)
    }

    for {
      result <- Future.foldLeft(
        userIds.map(id => sendNotification(id, groupId, notif))
      )(true)((a, b) => a)
    } yield result

  }

  private def sendNotificationsOnMeeting(groupId: Long, timeAt: Instant) = {

    val getUsers = handler.getGroupsUsers(groupId)
    for {
      groupName <- handler.getGroupById(groupId).map(_.groupname)
      userIds <- getUsers
      result <- sendNotifications(
        groupId,
        userIds,
        s"""A group called "$groupName" has just announced a meeting on $timeAt. Feel free to join!"""
      )
    } yield result

  }

  private def SendNotificationOnGroup(
      groupId: Long,
      topics: Option[Seq[String]]
  ) = {

    val findUsersByTopics = {
      topics match {
        case None         => Future.successful(Seq())
        case Some(topics) => handler.getUsersByTopics(topics)
      }
    }

    for {
      groupName <- handler.getGroupById(groupId).map(_.groupname)
      userIds <- findUsersByTopics
      result <- sendNotifications(
        groupId,
        userIds,
        s"""A group called "$groupName" was just created. It suits your topics so check it out!"""
      )
    } yield result

  }

  private def foldToGropupsRepr(groups: Seq[Group]) = {
    Future
      .foldLeft(groups.map(group => mapFromGroup(group)))(Seq[GroupsRepr]()) {
        (a, b) => b +: a
      }
  }

}
