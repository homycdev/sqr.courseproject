package petmeeter.data

import petmeeter._
import petmeeter.data.Tables._
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.{ExecutionContext, Future}

class SlickDataHandler(db: Database)(implicit ec: ExecutionContext)
    extends DataHandler {

  override def createUser(email: String, hash: String): Future[Long] = {

    val user = User(id = 0, email = email, hashed = hash)
    val addUser = UserTable returning UserTable.map(_.id) += user
    val ifEmailExists = findUserByEmail(email).flatMap {
      case None    => DBIO.successful(true)
      case Some(_) => DBIO.failed(EmailAlreadyExistsException())
    }
    val query = ifEmailExists.flatMap { _ => addUser }
    db.run(query)
  }

  override def getAllUsers(): Future[Seq[User]] = {
    db.run(UserTable.result)
  }

  override def getUserByEmail(email: String): Future[User] = {

    val query = findUserByEmail(email).flatMap {
      case Some(user) => DBIO.successful(user)
      case None       => DBIO.failed(WrongCredentialsException())

    }

    db.run(query)

  }

  override def getUserById(id: Long): Future[User] = {
    db.run(UserTable.filter(_.id === id).result.headOption)
      .flatMap {
        case Some(user) => Future.successful(user)
        case None       => Future.failed(UserNotFoundException())
      }
  }

  override def updateUser(user: User): Future[User] = {

    val findUser = ifUserExists(user.id)

    def ifEmailExists(originalUser: User) = {
      if (originalUser.email == user.email) {
        DBIO.successful(None)
      } else
        UserTable.filter(_.email === user.email).result.headOption.flatMap {
          case None    => DBIO.successful(true)
          case Some(_) => DBIO.failed(EmailAlreadyExistsException())
        }
    }

    val update = UserTable.filter(_.id === user.id).update(user)

    val query = findUser
      .flatMap { originalUser => ifEmailExists(originalUser) }
      .flatMap(_ => update)

    db.run(query).map(_ => user)

  }

  override def getAllGroups(): Future[Seq[Group]] = {
    db.run(GroupTable.result)
  }

  override def getGroupById(id: Long): Future[Group] = {
    val ifGroupExists = GroupTable.filter(_.id === id).result.headOption

    val query = ifGroupExists.flatMap {
      case Some(group) => DBIO.successful(group)
      case None        => DBIO.failed(GroupNotFoundException())
    }
    db.run(query)

  }

  override def findGroupsByPattern(pattern: String): Future[Seq[Group]] = {
    val query = GroupTable.filter(_.groupname.like(pattern)).result
    db.run(query)
  }

  override def getUserTopicsById(id: Long): Future[Seq[Topic]] = {

    val getTags = UsertopicTable.filter(
      _.userid === id
    ) join TopicTable on (_.topicid === _.id) map {
      case (usertags, tags) => tags
    }
    val query = ifUserExists(id).flatMap { _ => getTags.result }
    db.run(query)
  }

  override def getUserGroupsById(id: Long): Future[Seq[Group]] = {

    val getGroups = UsergroupTable.filter(
      _.userid === id
    ) join GroupTable on (_.groupid === _.id) map (_._2)
    val query = ifUserExists(id).flatMap { _ => getGroups.result }

    db.run(query)
  }

  override def getGroupTopicsById(id: Long): Future[Seq[Topic]] = {

    val groupTags = (GrouptopicTable.filter(
      _.groupid === id
    ) join TopicTable on (_.topicid === _.id) map (_._2)).result

    val query = ifGroupExists(id).flatMap { _ => groupTags }

    db.run(query)
  }

  override def createGroup(group: Group, founderId: Long): Future[Long] = {

    val ifGroupDoesntExist = GroupTable
      .filter(_.groupname === group.groupname)
      .result
      .headOption
      .flatMap {
        case None    => DBIO.successful(true)
        case Some(_) => DBIO.failed(NameAlreadyExistsException())
      }

    val create = for {
      id <- GroupTable returning GroupTable.map(_.id) += group
      _ <- UsergroupTable returning UsergroupTable.map(_.groupid) += Usergroup(
        founderId,
        id
      )
    } yield id

    val query = ifUserExists(founderId)
      .flatMap(_ => ifGroupDoesntExist)
      .flatMap(_ => create)

    db.run(query.transactionally)
  }

  private def manageGroup(action: DBIO[_], userId: Long, groupId: Long) = {
    val ifUserExists = UserTable.filter(_.id === userId).result.headOption

    ifUserExists.flatMap {
      case None => DBIO.failed(UserNotFoundException())
      case Some(_) =>
        ifGroupExists(groupId)
          .flatMap { _ =>
            action
          }
    }
  }

  override def joinGroup(groupId: Long, userId: Long): Future[Boolean] = {
    val join = UsergroupTable += Usergroup(userId, groupId)
    val query = manageGroup(join, userId, groupId)
    db.run(query)
      .flatMap(_ => Future.successful(true))

  }

  override def leaveGroup(groupId: Long, userId: Long): Future[Boolean] = {
    val delete = UsergroupTable
      .filter(_.groupid === groupId)
      .filter(_.userid === userId)
      .delete
    val query = manageGroup(delete, userId, groupId)
    db.run(query)
      .flatMap(_ => Future.successful(true))
  }

  override def getGroupsByTopics(topics: Seq[String]): Future[Seq[Group]] = {

    val topicIds = DBIO.fold(
      topics.map { topic =>
        TopicTable.filter(_.name === topic).map(_.id).result
      },
      Seq()
    ) { (total, ids) =>
      total.concat(ids)
    }

    def groupSets(Ids: Seq[Long]) =
      Ids.map { id =>
        (GrouptopicTable.filter(
          _.topicid === id
        ) join GroupTable on (_.groupid === _.id))
          .map(_._2)
          .result
          .map(_.toSet)
      }

    val query = topicIds.flatMap { seq =>
      {
        groupSets(seq)
          .reduce((A, B) => B.flatMap(b => A.map(a => b.intersect(a))))
          .map(_.toSeq)
      }
    }

    db.run(query).map(_.toSeq)
  }

  override def getAllComments(groupId: Long): Future[Seq[Comment]] = {

    val comments = CommentTable.filter(_.groupid === groupId).result
    val query = ifGroupExists(groupId).flatMap(_ => comments)

    db.run(query)
  }

  override def addComment(comment: Comment): Future[Boolean] = {

    val query = ifGroupExists(comment.groupid)
      .flatMap { _ => ifUserExists(comment.userid) }
      .flatMap { _ => CommentTable += comment }

    db.run(query)
      .flatMap(_ => Future.successful(true))
  }

  override def getUsersByTopics(topics: Seq[String]): Future[Seq[Long]] = {

    val topicIds = DBIO.fold(
      topics.map { topicName =>
        TopicTable.filter(_.name === topicName).map(_.id).result
      },
      Seq()
    ) { (total, users) =>
      total.concat(users)
    }

    def getUsers(ids: Seq[Long]) =
      DBIO.fold(
        ids.map { id =>
          (UsertopicTable.filter(
            _.topicid === id
          ) join UserTable on (_.userid === _.id) map (_._2.id)).result
            .map(_.toSet)
        },
        Set()
      ) { (total, users) =>
        total.concat(users)
      }

    val query = for {
      ids <- topicIds
      users <- getUsers(ids)
    } yield users

    db.run(query).map(_.toList)

  }

  override def updateUserTopics(
      topics: Seq[String],
      id: Long
  ): Future[Seq[Topic]] = {
    // Нормально ли тут все с конкарренси??? По идее менять тэги юзеру может только юзер,
    // Так что в одни и те же данные мы попасть не можем, но если много юзеров сразу меняют себе теги
    // То получается много удалений из UsertagsTable

    val deleteInUserTags = UsertopicTable.filter(_.userid === id).delete
    val insertIntoUserTags = topics.map { topic =>
      for {
        tagId <- TopicTable.filter(_.name === topic).result.headOption.map {
          case Some(value) => value.id
        }
        _ <- UsertopicTable += Usertopic(id, tagId)
      } yield ()
    }

    val updateInUserTags = deleteInUserTags.flatMap { _ =>
      DBIO.sequence(insertIntoUserTags)
    }

    val updateTags = topics.map { topic =>
      val ifTagInStorage = TopicTable.filter(_.name === topic).result.headOption
      ifTagInStorage.flatMap {
        case None    => TopicTable += Topic(1, topic)
        case Some(_) => DBIO.successful(topic)
      }
    }
    val completeAction =
      DBIO.sequence(updateTags).flatMap { _ => updateInUserTags }

    val query = ifUserExists(id)
      .flatMap { _ => completeAction }
      .flatMap(_ =>
        (UsertopicTable.filter(
          _.userid === id
        ) join TopicTable on (_.topicid === _.id) map (_._2)).result
      )
    db.run(query.transactionally)
  }

  override def updateGroupTopics(
      topics: Seq[String],
      groupId: Long
  ): Future[Seq[Topic]] = {

    val deleteInGroupTags = GrouptopicTable.filter(_.groupid === groupId).delete
    val insertIntoGroupTags = topics.map { topic =>
      for {
        topicId <- TopicTable.filter(_.name === topic).result.headOption.map {
          case Some(value) => value.id
        }
        _ <- GrouptopicTable += Grouptopic(groupId, topicId)
      } yield ()
    }

    val updateInGroupTopics = deleteInGroupTags.flatMap { _ =>
      DBIO.sequence(insertIntoGroupTags)
    }

    val updateTags = topics.map { topic =>
      val ifTagInStorage = TopicTable.filter(_.name === topic).result.headOption
      ifTagInStorage.flatMap {
        case None    => TopicTable += Topic(0, topic)
        case Some(_) => DBIO.successful(topic)
      }
    }
    val completeAction =
      DBIO.sequence(updateTags).flatMap { _ => updateInGroupTopics }

    val query = ifGroupExists(groupId)
      .flatMap { _ => completeAction }
      .flatMap(_ =>
        (GrouptopicTable.filter(
          _.groupid === groupId
        ) join TopicTable on (_.topicid === _.id)).map(_._2).result
      )
    db.run(query.transactionally)
  }

  override def getAllUserMessages(userId: Long): Future[Seq[Message]] = {

    val getAllMessages = MessageTable
      .filter(message =>
        message.senderid === userId || message.receiverid === userId
      )
      .result
    val query = ifUserExists(userId).flatMap(_ => getAllMessages)
    db.run(query)
  }

  override def getUserNotifications(userId: Long): Future[Seq[Notification]] = {

    val getAllNotifs = NotificationTable.filter(_.userid === userId).result

    val query = for {
      _ <- ifUserExists(userId)
      result <- getAllNotifs
    } yield result

    db.run(query)
  }

  override def sendMessage(message: Message): Future[Boolean] = {

    val ifSenderExists = ifUserExists(message.senderid)

    val ifReceiverExists = ifUserExists(message.receiverid)

    val send = MessageTable += message

    val query = for {
      _ <- ifSenderExists
      _ <- ifReceiverExists
      _ <- send
    } yield true
    db.run(query)
  }

  override def sendNotification(notification: Notification): Future[Boolean] = {

    val addNotif = NotificationTable += notification

    val query = for {
      _ <- ifUserExists(notification.userid)
      _ <- ifGroupExists(notification.groupid)
      _ <- addNotif
    } yield true

    db.run(query)

  }

  override def createMeeting(meeting: Meeting): Future[Boolean] = {

    val ifMeetingExists = MeetingTable
      .filter(m =>
        m.groupid === meeting.groupid && m.description === meeting.description
      )
      .result
      .headOption
      .flatMap {
        case None    => DBIO.successful(true)
        case Some(_) => DBIO.failed(NotAValidMeetingExeption())
      }

    val query = for {
      _ <- ifMeetingExists
      _ <- ifGroupExists(meeting.groupid)
      _ <- MeetingTable += meeting
    } yield true

    db.run(query)

  }

  override def getGroupsUsers(groupId: Long): Future[Seq[Long]] = {
    val query = for {
      _ <- ifGroupExists(groupId)
      result <- (UsergroupTable.filter(
          _.groupid === groupId
        ) join UserTable on (_.userid === _.id) map (_._2.id)).result
    } yield result

    db.run(query)
  }

  // ----------------------------INTERNAL-------------------------------------------
  private def findUserById(id: Long) =
    UserTable.filter(_.id === id).result.headOption

  private def findUserByEmail(email: String) =
    UserTable.filter(_.email === email).result.headOption

  private def findGroupById(id: Long) =
    GroupTable.filter(_.id === id).result.headOption

  private def ifUserExists(userId: Long) = {

    findUserById(userId).flatMap {
      case None               => DBIO.failed(UserNotFoundException())
      case Some(originalUser) => DBIO.successful(originalUser)
    }
  }

  private def ifGroupExists(groupId: Long) = {
    findGroupById(groupId).flatMap {
      case None    => DBIO.failed(GroupNotFoundException())
      case Some(_) => DBIO.successful(true)
    }
  }

}
